package main

import "golang.org/x/net/websocket"
import "net/http"

//import "sync"
import "fmt"
import "time"
import "encoding/json"
import "os"
import "strings"
import "strconv"

type Participant struct {
	toClient            chan Event //Nil if the client is not connected
	ActionsLeft         int64      //Number of actions the participant can still do this round
	Question, Expertise int64
	ClientAddress       string
}

type ParticipantList map[int64]*Participant

func (p *Participant) MarshalJSON() ([]byte, error) {
	var enc string
	enc = "{\"ClientConnected\":" + fmt.Sprint(p.toClient != nil) + "," +
		"\"ActionsLeft\":" + fmt.Sprint(p.ActionsLeft) + "," +
		"\"Question\":" + fmt.Sprint(p.Question) + "," +
		"\"Expertise\":" + fmt.Sprint(p.Expertise) + "," +
		"\"ClientAddress\":\"" + p.ClientAddress + "\"}"
	return []byte(enc), nil
}

func (pl ParticipantList) MarshalJSON() ([]byte, error) {
	if len(pl) == 0 {
		return []byte{'{', '}'}, nil
	}
	keys := make([][]byte, 0, len(pl))
	vals := make([][]byte, 0, len(pl))
	total := 1 + len(pl)*4
	for uid, participant := range pl {
		key, errk := json.Marshal(uid)
		if errk != nil {
			return nil, errk
		}
		keys = append(keys, key)
		val, errv := json.Marshal(participant)
		if errv != nil {
			return nil, errv
		}
		vals = append(vals, val)
		total += len(key) + len(val)
	}
	enc := make([]byte, 1, total)
	enc[0] = '{'
	for idx := range keys {
		enc = enc[0 : len(enc)+1]
		enc[len(enc)-1] = '"'
		enc = enc[0 : len(enc)+len(keys[idx])]
		copy(enc[len(enc)-len(keys[idx]):len(enc)], keys[idx])
		enc = enc[0 : len(enc)+2]
		enc[len(enc)-2] = '"'
		enc[len(enc)-1] = ':'
		enc = enc[0 : len(enc)+len(vals[idx])]
		copy(enc[len(enc)-len(vals[idx]):len(enc)], vals[idx])
		enc = enc[0 : len(enc)+1]
		enc[len(enc)-1] = ','
	}
	enc = enc[0:cap(enc)]
	enc[cap(enc)-1] = '}'
	return enc, nil
}

var game *Game

func init() {
	game = new(Game)
	game.History = make([]Event, 0, 100000)
	game.Participants = make(map[int64]*Participant)
	game.toEngine = make(chan Event)
}

func main() {
	historyFileName := "files/history"
	for {
		file, err := os.OpenFile(historyFileName, os.O_CREATE+os.O_EXCL+os.O_WRONLY, 0666)
		if err == nil {
			game.HistoryWriter = file
			break
		} else if os.IsExist(err) {
			historyFileName += "I"
			continue
		} else {
			fmt.Println("Could not create history file:", err)
			return
		}
	}

	//Setup websocket server
	http.Handle("/indexWS", websocket.Handler(wsClientServer))
	http.Handle("/adminWS", websocket.Handler(wsAdminServer))
	http.Handle("/beepWS", websocket.Handler(wsBeepServer))
	//Setup file server
	http.Handle("/", http.FileServer(http.Dir("./files")))

	//Start game engine

	go gameEngine(game)

	//Start serving
	err := http.ListenAndServeTLS(":8080","cert.pem", "key.pem", nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}

func (g *Game) Continue(forceNewGame bool) {
	//If the game did not end already
	if g.Settings.StartedGames>0 && g.Settings.FinishedRounds < g.Settings.NumberRounds {
		//End this round
		g.SendLastRound()
		g.Settings.ActionsLeft = 0
		for _, userData := range g.Participants {
			userData.ActionsLeft = 0
		}
		g.Settings.FinishedRounds++
		//A round ended, start the next game if it was the last one and autostart is active
		if g.Settings.FinishedRounds >= g.Settings.NumberRounds && g.Settings.AutostartNewGame {
			g.StartNewGame()
			return
		}
		//A round ended and it wasn't the last one start a new round if forceNewGame is not set
		if g.Settings.NumberRounds > g.Settings.FinishedRounds && !forceNewGame {
			g.StartNewRound()
			return
		}
	}
	//If forceNewGame is set, start a new game
	if forceNewGame {
		g.StartNewGame()
	} else {
	  g.BroadcastToBeep(Event{})
	}
}

func gameEngine(g *Game) {
	for {
		//Game automatic logic
		g.Lock.Lock()
		if g.Settings.ActionsLeft == 0 {
			g.Continue(false)
		}
		g.Lock.Unlock()

		//Wait for events and process them
		select {
		case event, open := <-g.toEngine:
			g.Lock.Lock()
			if open {
				//Got a message from the client, put a timestamp and deal with it
				event.Time = time.Now()
				switch event.EventId {
				case EventActionInquire, EventActionConfirm, EventActionRefer, EventActionNegate, EventActionSkip:
				        //Enforce message limit
				        sender := g.Participants[event.SenderUid]
				        if sender.ActionsLeft <= 0 {
					  break
					}
					//Check if this message is valid in the current situation
					event.Invalid = !g.ValidateAction(event)
					if event.Invalid {
						//The action was invalid for whatever reason. Send it back and stop processing
						sender.Send(event)
						g.HistoryAppend(event)
						break
					}
					//Canonicalize message (add expertise and question if relevant, reset irrelevant fields)
					event = g.CanonicalizeAction(event)
					event = g.SetActionReward(event)
					sender.ActionsLeft--
					g.Settings.ActionsLeft--
					sender.Send(event)
					g.HistoryAppend(event)
				default:
					fmt.Println("Unknown EventId for Action:", event)
				}
			}
			g.Lock.Unlock()
		}
	}
}

const wsDebug = false

func wsAdminServer(ws *websocket.Conn) {
	g := game
	defer ws.Close()
	//New connection coming in, check password
	var password string
	err := websocket.JSON.Receive(ws, &password)
	if err != nil {
		fmt.Println("AdminServer: Receiving password failed with error: " + err.Error())
		return
	} else if password != "r2d2c3po" {
		fmt.Println("AdminServer: Wrong password. ")
		return
	}
	fmt.Println("AdminServer: Successfully authenticated admin. ")
	//Send current state and wait for instructions
	for {
		//Send information about current game state
		g.Lock.Lock()
		err = websocket.JSON.Send(ws, &g)
		g.Lock.Unlock()
		if err != nil {
			fmt.Println("AdminServer: Send failed with error: " + err.Error())
			return
		}
		//Receive command
		var cmd string
		err = websocket.JSON.Receive(ws, &cmd)
		if err != nil {
			fmt.Println("AdminServer: Receive failed with error: " + err.Error())
			return
		}
		//Handle command
		if cmd != "Refresh" {
			fmt.Println("AdminServer: Receive Command:", cmd)
			switch {
			case cmd == "CommandStart":
				g.Lock.Lock()
				g.Continue(true)
				g.Lock.Unlock()
			case strings.HasPrefix(cmd, "CommandEndRound"):
				parseNum, parseErr := strconv.ParseInt(strings.TrimPrefix(cmd, "CommandEndRound"), 10, 64)
				if parseErr != nil {
					fmt.Println("AdminServer: Parsing round number failed with error: " + parseErr.Error())
					return
				}
				g.Lock.Lock()
				if parseNum == g.Settings.FinishedRounds {
					g.Continue(false)
				} else {
					fmt.Println("AdminServer: Round number in CommandEndRound didn't match current round.")
				}
				g.Lock.Unlock()
			case strings.HasPrefix(cmd, "CommandNextNumberRounds"):
				parseNum, parseErr := strconv.ParseInt(strings.TrimPrefix(cmd, "CommandNextNumberRounds"), 10, 64)
				if parseErr != nil {
					fmt.Println("AdminServer: Parsing round number failed with error: " + parseErr.Error())
					return
				}
				g.Lock.Lock()
				g.Settings.NextNumberRounds = parseNum
				g.Lock.Unlock()
			case cmd == "CommandToggleAutostartNewGame":
				g.Lock.Lock()
				g.Settings.AutostartNewGame = !g.Settings.AutostartNewGame
				g.Lock.Unlock()
			default:
				fmt.Println("AdminServer: Unknown command", cmd)
			}
		}
	}
}

func wsBeepServer(ws *websocket.Conn) {
	g := game
	defer ws.Close()
	//Register channel
	toBuffer := make(chan BeepEvent)
	fromBuffer := make(chan BeepEvent)
	go BeepEventBuffer(toBuffer, fromBuffer)
	g.Lock.Lock()
	g.toBeep = append(g.toBeep, toBuffer)
	history := g.History
	g.Lock.Unlock()

	//Start listening for commands
	go func() {
		defer ws.Close()
		for {
			var command Command
			err := websocket.JSON.Receive(ws, &command)
			if err == nil {
				g.Lock.Lock()
				if g.Settings.FinishedRounds != command.CurrentRound || g.Settings.StartedGames != command.CurrentGame {
					fmt.Println("BeepServer: Received command for wrong game or round:", command)
					g.Lock.Unlock()
					continue
				}
				g.Lock.Unlock()
				command.Event.Admin = true
				command.Time = time.Now()
				//Execute commands
				switch command.CommandId {
				case CommandEndRound:
					g.Lock.Lock()
					g.Continue(false)
					g.Lock.Unlock()
				case CommandStartGame:
					g.Lock.Lock()
					g.Continue(true)
					g.Lock.Unlock()
				case CommandAddUser:
					g.Lock.Lock()
					uid := g.RegisterNewParticipant()
					if uid > 0 {
						fmt.Println("BeepServer: Registered new participant with uid:", uid)
					} else {
						fmt.Println("BeepServerError: Registering new participant failed (uid:", uid, "). Probably because game is running?")
					}
					g.Lock.Unlock()
				case CommandInjectAction:
					g.Lock.Lock()
					g.toEngine <- command.Event
					g.Lock.Unlock()
				case CommandToggleAutostartGames:
					g.Lock.Lock()
					g.Settings.AutostartNewGame=!g.Settings.AutostartNewGame
					g.BroadcastToBeep(Event{})
					g.Lock.Unlock()
				case CommandSetNextNumberRounds:
					g.Lock.Lock()
					if command.NextRounds>=0 {
					  g.Settings.NextNumberRounds=command.NextRounds
					} else {
					  fmt.Println("BeepServerError: Setting a negative number of rounds (",command.NextRounds,") is not possible.")
					}
					g.BroadcastToBeep(Event{})
					g.Lock.Unlock()
				default:
					fmt.Println("BeepServer: Received unknown command:", command)
				}

			} else {
				fmt.Println("BeepServer: Receiving failed with error: " + err.Error())
				g.Lock.Lock()
				for idx := range g.toBeep {
					if g.toBeep[idx] == toBuffer {
						g.toBeep[idx] = g.toBeep[len(g.toBeep)-1]
						g.toBeep[len(g.toBeep)-1] = nil
						g.toBeep = g.toBeep[0 : len(g.toBeep)-1]
						close(toBuffer)
						break
					}
				}
				g.Lock.Unlock()
				fmt.Println("BeepServer: CommandReceiver stopped.")
				return
			}
		}
	}()

	var err error
	//Send old history content
	if err == nil {
		for _, event := range history {
			err = websocket.JSON.Send(ws, BeepEvent{Event:event,Settings:g.Settings})
			if err != nil {
				break
			}
		}
	}
	//Start collecting stuff from buffer and sending it
	if err == nil {
		for event := range fromBuffer {
			err = websocket.JSON.Send(ws, event)
			if err != nil {
				break
			}
		}
	}
	//If a send failed, close outgoing channel and remove it from g.toBeep slice
	if err != nil {
		fmt.Println("BeepServer: Sending failed with error: " + err.Error())
		g.Lock.Lock()
		for idx := range g.toBeep {
			if g.toBeep[idx] == toBuffer {
				g.toBeep[idx] = g.toBeep[len(g.toBeep)-1]
				g.toBeep[len(g.toBeep)-1] = nil
				g.toBeep = g.toBeep[0 : len(g.toBeep)-1]
				close(toBuffer)
				break
			}
		}
		g.Lock.Unlock()
	}

	//Done, drain buffer to allow buffer routine to shutdown
	for _ = range fromBuffer {
	}

	fmt.Println("BeepServer: EventSender stopped.")
	return
}

func (g *Game) RegisterNewParticipant() (uid int64) {
	if g.Settings.StartedGames != 0 {
		return -1
	}
	for uid = 1017; ; uid+=17 {
		if _, ok := g.Participants[uid]; !ok {
			//Make an entry in the map of participants
			participant := new(Participant)
			g.Participants[uid] = participant
			g.Settings.ActiveParticipants++
			event := Event{EventId: EventUserJoined, ReferredUid: uid, Time: time.Now(), Reward:RewardJoin}
			g.Settings.TotalReward += event.Reward
			g.Settings.NumberRounds = 6 //TODO: Adapt to number of players
			g.Broadcast(event)
			g.HistoryAppend(event)
			break
		}
	}
	return
}

func wsClientServer(ws *websocket.Conn) {
	g := game
	defer ws.Close()
	//Wait for client to send his user id (0 is a request for a new user id)
	var uid int64
	err := websocket.JSON.Receive(ws, &uid)
	if err != nil {
		fmt.Println("ClientServer: Receiving user id failed with error: " + err.Error())
		return
	}
	//Check if the login request was valid and log participant in if it was
	g.Lock.Lock()
	var participant *Participant
	var ok bool
	if participant, ok = g.Participants[uid]; !ok {
		fmt.Println("ClientServerError: Requested user id does not exist")
		g.Lock.Unlock()
		return
	}
	//Login request was valid
	//Register by setting up channel (replace the old one if necessary)
	g.Settings.LoggedInParticipants++
	toBuffer := make(chan Event)
	if participant.toClient != nil {
		close(participant.toClient)
		g.Settings.LoggedInParticipants--
		participant.toClient = nil
	}
	participant.toClient = toBuffer
	participant.ClientAddress = ws.Request().RemoteAddr
	//Save current history slice for replay and hand back control to the gameserver
	history := g.History
	g.Lock.Unlock()

	//Collect incoming stuff in a buffer to avoid gameserver lockup
	fromBuffer := make(chan Event)
	go EventBuffer(toBuffer, fromBuffer)

	//Pass everything that is received from the client to the engine
	go func() {
		for {
			var event Event
			err := websocket.JSON.Receive(ws, &event)
			if err == nil {
				//Force correct sender
				event.SenderUid = uid
				event.Admin = false
				g.toEngine <- event
			} else {
				fmt.Println("ClientServer: Receiving failed with error: " + err.Error())
				g.Lock.Lock()
				if participant.toClient != nil && participant.toClient == toBuffer {
					close(participant.toClient)
					g.Settings.LoggedInParticipants--
					participant.toClient = nil
				}
				g.Lock.Unlock()
				fmt.Println("ClientServer: EventReceiver stopped.")
				return
			}
		}
	}()

	//Send user id and history to client
	err = websocket.JSON.Send(ws, uid)
	if err == nil {
		//Find beginning of current round in history
		var currentIdx int
		for currentIdx = len(history) - 1; currentIdx >= 0; currentIdx-- {
			if history[currentIdx].EventId == EventNextRound || history[currentIdx].EventId == EventNextGame {
				break
			}
		}
		//Send everything from old rounds and cut out received messages from last round
		for idx, event := range history {
		        if event.Invalid {continue}
			if (event.SenderUid == uid || (idx <= currentIdx && event.ReceiverUid == uid) || (event.ReceiverUid == 0 && event.SenderUid == 0)) {
				err = websocket.JSON.Send(ws, event)
				if err != nil {
					break
				}
			}
		}
	}

	//Start collecting stuff from buffer and sending it
	if err == nil {
		for event := range fromBuffer {
			err = websocket.JSON.Send(ws, event)
			if err != nil {
				break
			}
			fmt.Println("Sent:", event)
		}
	}

	//If a send failed, close outgoing channel
	if err != nil {
		fmt.Println("ClientServer: Sending failed with error: " + err.Error())
		g.Lock.Lock()
		if participant.toClient != nil && participant.toClient == toBuffer {
			close(participant.toClient)
			g.Settings.LoggedInParticipants--
			participant.toClient = nil
		}
		g.Lock.Unlock()
	}

	//Done, drain buffer to allow buffer routine to shutdown
	for _ = range fromBuffer {
	}

	fmt.Println("ClientServer: EventSender stopped.")
	return
}

type eventBufferElement struct {
	event Event
	next  *eventBufferElement
}

func EventBuffer(in, out chan Event) {
	var next *eventBufferElement
	for {
		for c := next; c != nil; c = c.next {
			fmt.Println("  ", c.event)
		}
		if next == nil {
			//The buffer is empty, try sending incoming stuff immediately, otherwise start to buffer
			if event, open := <-in; open {
				select {
				case out <- event:
					fmt.Println("BufferDebug: Skip buffer") //Sent it
				default:
					fmt.Println("BufferDebug: Start buffer") //Can't send, start a buffer
					next = &eventBufferElement{event: event}
				}
			} else {
				//Incoming channel was closed, close the outgoing one and exit
				close(out)
				fmt.Println("EventBuffer: Stopped.")
				return
			}
		} else {
			//There is buffered stuff, try sending something from the buffer and append incoming
			select {
			case out <- next.event:
				fmt.Println("BufferDebug: From buffer") //Sent something, get rid of first element in buffer
				next = next.next
			case event, open := <-in:
				fmt.Println("BufferDebug: Append buffer") //Received something, append to buffer if it was an event
				if open {
					//Find end of buffer and append an element
					var last *eventBufferElement
					for last = next; last.next != nil; last = last.next {
					}
					last.next = &eventBufferElement{event: event}
				} else {
					//Incoming channel was closed, there is nothing left to do but to send the buffer
					for ; next != nil; next = next.next {
						out <- next.event
					}
				}
			}
		}
	}
}


//Same code as above
type beepEventBufferElement struct {
	event BeepEvent
	next  *beepEventBufferElement
}

func BeepEventBuffer(in, out chan BeepEvent) {
	var next *beepEventBufferElement
	for {
		for c := next; c != nil; c = c.next {
			fmt.Println("  ", c.event)
		}
		if next == nil {
			//The buffer is empty, try sending incoming stuff immediately, otherwise start to buffer
			if event, open := <-in; open {
				select {
				case out <- event:
					fmt.Println("BufferDebug: Skip buffer") //Sent it
				default:
					fmt.Println("BufferDebug: Start buffer") //Can't send, start a buffer
					next = &beepEventBufferElement{event: event}
				}
			} else {
				//Incoming channel was closed, close the outgoing one and exit
				close(out)
				fmt.Println("EventBuffer: Stopped.")
				return
			}
		} else {
			//There is buffered stuff, try sending something from the buffer and append incoming
			select {
			case out <- next.event:
				fmt.Println("BufferDebug: From buffer") //Sent something, get rid of first element in buffer
				next = next.next
			case event, open := <-in:
				fmt.Println("BufferDebug: Append buffer") //Received something, append to buffer if it was an event
				if open {
					//Find end of buffer and append an element
					var last *beepEventBufferElement
					for last = next; last.next != nil; last = last.next {
					}
					last.next = &beepEventBufferElement{event: event}
				} else {
					//Incoming channel was closed, there is nothing left to do but to send the buffer
					for ; next != nil; next = next.next {
						out <- next.event
					}
				}
			}
		}
	}
}
