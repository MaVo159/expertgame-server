svgNS = "http://www.w3.org/2000/svg"
xlinkNS = 'http://www.w3.org/1999/xlink'

window.onload = function () {
  document.getElementById("GenerateButton").onclick = function() {
    generate(document.getElementById("NumPages").value,document.getElementById("NumPlayers").value)
  }
  document.getElementById("HideUIButton").onclick = function() {
    document.getElementById("UI").style.display="none"
  }
};

function generate(numPages, numPlayers) {
var oldList = document.getElementsByTagNameNS(svgNS,'svg')
while (oldList.length!=0) {
  document.body.removeChild(oldList[0])
  oldList = document.getElementsByTagNameNS(svgNS,'svg')
}

var numNames =numPlayers-1
if (numPages==0) {
  drawPage(200,surnames.slice(0,numNames))
  return
}

for (var p=1;p<=numPages;p++) {
  var listNames = [];
  for (var n=0;n<numNames;n++) {
    listNames.push(surnames[(n+numNames*p)%surnames.length])
  }
  drawPage(200,listNames)
}
};

function drawPage(width,names){
 var height = width*1.3
 var hwdiff = height-width
 var svg=document.createElementNS(svgNS, "svg")
 svg.setAttribute('xmlns:xlink',xlinkNS)
 svg.style.width=width+"mm"
 svg.style.height=height+"mm"
 document.body.appendChild(svg)

 //Draw key
 var keyList = ["I = Inquiry","R = Referral","C = Confirmation","N = Negation"]
 for (var i=0.0; i<keyList.length; i++) {
 var text = document.createElementNS(svgNS,"text")
 text.setAttributeNS(null,"x",10+"mm")
 text.setAttributeNS(null,"y",10+(hwdiff-20)/keyList.length*i+"mm")
 text.setAttributeNS(null,"font-size",20)
 text.setAttributeNS(null,"text-anchor","start")
 text.appendChild(document.createTextNode(keyList[i]))
 svg.appendChild(text)
 }
 var text = document.createElementNS(svgNS,"text")
 text.setAttributeNS(null,"x",10+(width-20)/2+"mm")
 text.setAttributeNS(null,"y",10+"mm")
 text.setAttributeNS(null,"font-size",20)
 text.setAttributeNS(null,"text-anchor","start")
 text.appendChild(document.createTextNode("Document your messages with arrows:"))
 svg.appendChild(text)
 var image = document.createElementNS(svgNS,"image")
 image.setAttributeNS(null,"x",10+(width-20)/2+"mm")
 image.setAttributeNS(null,"y",20+"mm")
 image.setAttributeNS(xlinkNS,"href","FormKeyPicture.svg")
 image.setAttributeNS(null,"width",(width-20)/2+"mm")
 image.setAttributeNS(null,"height",(width-20)/2*0.3+"mm")
 svg.appendChild(image)

 //Draw player in the middle
 var text = document.createElementNS(svgNS,"text")
 text.setAttributeNS(null,"x",width/2+"mm")
 text.setAttributeNS(null,"y",hwdiff+width/2+"mm")
 text.setAttributeNS(null,"font-size",20)
 text.appendChild(document.createTextNode("You"))
 svg.appendChild(text)

 //Draw names in a circle around player
 for (var i=0.0; i<names.length; i++) {
 var text = document.createElementNS(svgNS,"text")
 text.setAttributeNS(null,"x",width/2+(Math.sin(i/names.length*2*Math.PI))*0.8*width/2+"mm")
 text.setAttributeNS(null,"y",width/2-(Math.cos(i/names.length*2*Math.PI))*0.8*width/2+hwdiff+"mm")
 text.setAttributeNS(null,"font-size",20)
 text.setAttributeNS(null,"text-anchor","middle")
 
 text.appendChild(document.createTextNode(names[i]))
 svg.appendChild(text)
 }
}
