//Set to true to shuffle contact lists after every game (NO-ID control experiment)
var randomized = false;

EventActionInquire = 1
EventActionConfirm = 2
EventActionRefer = 3
EventActionNegate = 4
EventActionSkip = 5
EventNextRound = 100
EventNextGame = 101
EventUserJoined = 1000


var contacts;
var body;
var chats;
var ws
var ownUid
var expertiseData = {}
var questionsData = {}
var userData = {};
var messagesLeft = 0;
var focusedUid = 0;
var reward = 0
var gameNumber=0
var participantsNumber=0
var chatsUnread = 0
var invalidMessage = null
var rewardOverview
var mt

function resize() {
  var chats=document.getElementById('Chats')
  var actions=document.getElementById('Actions')
  var margins = 0
  margins+=Number(getComputedStyle(chats).marginTop.slice(0,-2))
  margins+=Number(getComputedStyle(chats).marginBottom.slice(0,-2))
  margins+=Number(getComputedStyle(actions).marginTop.slice(0,-2))
  margins+=Number(getComputedStyle(actions).marginBottom.slice(0,-2))
  chats.style.height=(window.innerHeight-actions.offsetHeight-(chats.offsetHeight-chats.clientHeight)-margins)+'px'
  setTimeout(function(){
    if (focusedUid!=0) {
      var chat = userData[focusedUid].Chat
      chat.scrollTop=chat.scrollHeight
    }
  },1)
}

window.onload = function () {
  //Initialize mersenne twister
  mt = new MersenneTwister19937; // creating an instance of MersenneTwister19937 class.
  mt.init_by_array(Array(0x123, 0x234, 0x345, 0x456, 210786), 5)

  window.onresize=function(){resize()}
  resize()
  //Initialize some variable for easy access
  body = document.getElementById('Body')
  contacts = document.getElementById('Contacts')
  chats = document.getElementById('Chats')
  rewardOverview = document.getElementById('RewardOverview')
  document.getElementById('ContactUser').onclick=function(){focusChat(0)}
  //Set initial focus
  focusChat(0)
  //Show loading popup
  var loadingDiv=document.createElement('div');
  loadingDiv.setAttribute("class","Popup");
  var loadingText = document.createElement('p')
  loadingText.innerHTML="Please wait while connecting to server..."
  loadingDiv.appendChild(loadingText)
  body.appendChild(loadingDiv);
  //Connect to websocket
  ws = new WebSocket("wss://"+window.location.host+"/indexWS");
  ws.onopen = function(){
    ws.onclose = function(){
      var errorDiv=document.createElement('div');
      errorDiv.setAttribute("class","Popup");
      var errorText=document.createElement('p');
      errorText.innerHTML="Lost connection, please reload the page";
      errorDiv.appendChild(errorText);
      body.appendChild(errorDiv);
    }
    //Show login form
    var newUserField=document.createElement('input');
    newUserField.type="number";
    newUserField.min=0;
    newUserField.max=100000;
    newUserField.step=1;
    newUserField.value=0;
    loadingDiv.appendChild(newUserField);
    newUserButton=document.createElement('input');
    newUserButton.type="button";
    newUserButton.id="newUserButton";
    newUserButton.value="Join game";
    loadingDiv.appendChild(newUserButton);
    body.appendChild(loadingDiv);
    loadingText.innerHTML="Type in your user id. Then press 'Join Game'.";
    newUserButton.onclick=function(){
      newUserButton.onclick=undefined;
      newUserButton.disabled=true;
      ws.send(JSON.stringify(parseInt(newUserField.value,10)))
      loadingText.innerHTML="Joining, waiting for response from server..."
      ws.onmessage=function(msg){
	var msgData = JSON.parse(msg.data);
	ownUid=msgData
	loadingText.innerHTML="Your user id is: "+ownUid+". Please write it down."
	delete msgData
	newUserButton.value="Start playing"
	newUserButton.disabled=false;
	loadingDiv.removeChild(newUserField)
	newUserButton.onclick=function(){
	  newUserButton.onclick=undefined
	  body.removeChild(loadingDiv)
	}
	ws.onmessage=function(msg){
	  var msgData = JSON.parse(msg.data);
	  applyEvent(msgData,false)
	}
      }
    }
  }
  //Setup actions for send buttons
  document.getElementById('ActionInquire').getElementsByTagName('input')[0].onclick=function(){
    ws.send(JSON.stringify({EventId:EventActionInquire,SenderUid:ownUid,ReceiverUid:focusedUid}));
  };
  document.getElementById('ActionConfirm').getElementsByTagName('input')[0].onclick=function(){
    ws.send(JSON.stringify({EventId:EventActionConfirm,SenderUid:ownUid,ReceiverUid:focusedUid}));
  };
  document.getElementById('ActionRefer').getElementsByTagName('input')[0].onclick=function(){
    ws.send(JSON.stringify({EventId:EventActionRefer,SenderUid:ownUid,ReceiverUid:focusedUid,ReferredUid:expertiseData[userData[focusedUid].Question]}));
  };
  document.getElementById('ActionNegate').getElementsByTagName('input')[0].onclick=function(){
    ws.send(JSON.stringify({EventId:EventActionNegate,SenderUid:ownUid,ReceiverUid:focusedUid}));
  };
  document.getElementById('ActionSkipButton').onclick=function(event){
    event.stopPropagation()
    ws.send(JSON.stringify({EventId:EventActionSkip,SenderUid:ownUid}));
  };
};


function addParticipant(uid) {
  participantsNumber++
  if (typeof(uid)!="number") {throw 'WrongTypeError: Expected number. Value is: '+JSON.stringify(uid)}
  if (uid==ownUid) {
    userData[uid]={'Expertise':0,'Question':0}
    updateContactLabel(ownUid,true)
    return
  }
  //Add the contact
  var contact = document.getElementById('ContactTemplate').cloneNode(true)
  contact.setAttribute('id','Contact'+uid)
  
  //Make copy, because function literals are evaluated at call time
  var _copy_uid = uid
  contact.onclick=function(){focusChat(_copy_uid)}
  contacts.appendChild(contact)
  
  var chat = document.getElementById('ChatTemplate').cloneNode(true)
  chat.setAttribute('id','Chat'+uid)
  userData[uid]={
    'Chat':chat,
    'Contact':contact,
    'Expertise':0,
    'Question':0,
    'NewMessages':false,
    'ReceivedInquire':false,
    'SentInquire':true,
    'SentNegate':false,
    'SentRefer':false,
    'SentConfirm':false,
    'ReferredTo':null
  }
  chats.appendChild(chat)
  //Sort participants and reassign names
  var uids=[];
  for (var key in userData) {
    if (key != ownUid) {
      uids.push(Number(key))
    }
  }
  uids.sort(function(a,b){return (2*Number((a<ownUid)==(b<ownUid))-1)*(a-b);});
  for (var idx=0;idx<uids.length;idx++) {
    contacts.removeChild(userData[uids[idx]].Contact);
    contacts.appendChild(userData[uids[idx]].Contact);
    userData[uids[idx]].Name=surnames[idx];
    updateContactLabel(uids[idx],true);
  }
}

function shuffleContactOrder() {
  //Make a list of all uids
  var uids=[];
  for (var key in userData) {
    if (key != ownUid) {
      uids.push(Number(key))
    }
  }
  //Shuffle the list
  for (var i=0; i<uids.length;i++) {
    var r=mt.genrand_int31()%(i+1);
    oldi = uids[i]
    oldr = uids[r]
    uids[r]=oldi
    uids[i]=oldr
  }
  //Rearrange all according to reshuffled list
  for (var idx=0;idx<uids.length;idx++) {
    contacts.removeChild(userData[uids[idx]].Contact);
    contacts.appendChild(userData[uids[idx]].Contact);
  }
}

function assignNewContactNames() {
  //Sort participants and reassign names
  var uids=[];
  for (var key in userData) {
    if (key != ownUid) {
      uids.push(Number(key))
    }
  }
  uids.sort(function(a,b){return (2*Number((a<ownUid)==(b<ownUid))-1)*(a-b);});
  for (var idx=0;idx<uids.length;idx++) {
    userData[uids[idx]].Name=surnames[(idx+uids.length*gameNumber)%surnames.length];
    updateContactLabel(uids[idx],true);
  }
}

function resetContactHistories() {
  chatsUnread=0
  for (var key in userData) {
    if (key != ownUid) {
      i=Number(key)
      while (userData[i].Chat.firstChild) {
	userData[i].Chat.removeChild(userData[i].Chat.firstChild)
      }
      userData[i].NewMessages=false
      updateContactLabel(i,true);
    }
  }
}

function applyEvent(event) {
  if (event.EventId==EventUserJoined) {
    //Participant joined - Add to game state, update interface
    addParticipant(event.ReferredUid)
  } else if (event.EventId==EventNextGame || event.EventId==EventNextRound) {
    //System message - Reset message counter, update game state, update interface
    handleSystemMessage(event)
  } else if (EventActionInquire<=event.EventId<=EventActionNegate) {
    //Message - Update game state, update interface
    handleMessage(event)
  } else {
    console.log("UNKNOWN EVENT:"+JSON.stringify(event))
  }
  handleReward(event)
}



function focusChat(uid) {
  if (typeof(uid)!="number") {throw 'WrongTypeError: Expected number. Value is: '+JSON.stringify(uid)}
  var previousFocusedUid = focusedUid
  focusedUid=uid
  if (previousFocusedUid!=0) {
    userData[previousFocusedUid].Chat.style.display = 'none'
    userData[previousFocusedUid].NewMessages = false
    updateContactLabel(previousFocusedUid,true)
  } else {
    rewardOverview.style.display = 'none'
  }
  if (focusedUid) {
    userData[uid].Chat.style.display = 'block'
    userData[uid].Chat.scrollTop=userData[uid].Chat.scrollHeight
    if (userData[uid].NewMessages) {
      chatsUnread--
    }
    userData[uid].NewMessages=false
    updateContactLabel(uid,true)
  } else {
    rewardOverview.style.display = 'block'
  }
  updateActionsPanel()
}

function updateActionsPanel() {
  //Disable everything
  var actionOverlay = document.getElementById('ActionOverlay')
  var dismissOverlayButton = document.getElementById('DismissOverlayButton')
  var actionOverlayText = actionOverlay.getElementsByTagName('p')[0]
  actionOverlayText.innerHTML = ""
  var inquireAction = document.getElementById('ActionInquire')
  var negateAction=document.getElementById('ActionNegate')
  var referAction=document.getElementById('ActionRefer')
  var confirmAction=document.getElementById('ActionConfirm')
  //Activate the right stuff
  if (focusedUid==ownUid||focusedUid==0) {
    actionOverlay.style.display="block"
    dismissOverlayButton.style.display="none"
    actionOverlayText.innerHTML=""
  } else {
    if (messagesLeft<=0) {
      actionOverlay.style.display="block"
      dismissOverlayButton.style.display="none"
      actionOverlayText.innerHTML="You have already sent a message this round. Please wait until the round ends."
    } else if (chatsUnread>0) {
      actionOverlay.style.display="block"
      dismissOverlayButton.style.display="none"
      actionOverlayText.innerHTML="You have unread messages. Please click on contacts with the \"New message\" icon <img src=\"Unread.svg\" style=\"height:20px\"/> and read the new messages."
    } else if (invalidMessage!=null) {
      actionOverlay.style.display="block"
      dismissOverlayButton.style.display="block"
      dismissOverlayButton.onclick = function() {
	document.getElementById('ActionOverlay').style.display="none"
	invalidMessage = null
      }
      actionOverlayText.innerHTML="You tried to send a "
      var receiverName = userData[invalidMessage.ReceiverUid].Name
      switch(invalidMessage.EventId) {
	case EventActionConfirm :
	  actionOverlayText.innerHTML+="confirmation <img src=\"Confirm.svg\" style=\"height:20px\"/> "
	  break
	case EventActionRefer :
	  actionOverlayText.innerHTML+="referral <img src=\"Refer.svg\" style=\"height:20px\"/> "
	  break
	case EventActionNegate :
	  actionOverlayText.innerHTML+="negation <img src=\"Negate.svg\" style=\"height:20px\"/> "
	  break
      }
      actionOverlayText.innerHTML+="to "+receiverName+". You can't send this message because "
      if (!userData[invalidMessage.ReceiverUid].ReceivedInquire) {
	actionOverlayText.innerHTML+="you have <b>not</b> received an inquiry <img src=\"Inquire.svg\" style=\"height:20px\"/> from "+receiverName+" in this game."
      } else {
	var expertUid = expertiseData[userData[invalidMessage.ReceiverUid].Question]
	switch(invalidMessage.EventId) {
	  case EventActionConfirm:
	    actionOverlayText.innerHTML+="you are <b>not</b> the expert on "+receiverName+"'s question."
	    break
	  case EventActionRefer:
	    if (expertUid==ownUid) {
	      actionOverlayText.innerHTML+="<b>you</b> are the expert on "+receiverName+"'s question."
	    } else {
	      actionOverlayText.innerHTML+="you do <b>not</b> know who the expert on "+receiverName+"'s question is."
	    }
	    break
	  case EventActionNegate:
	    if (expertUid==ownUid) {
	      actionOverlayText.innerHTML+="you are the expert on "+receiverName+"'s question."
	    } else {
	      actionOverlayText.innerHTML+="you <b>do</b> know who the expert on "+receiverName+"'s question is."
	    }
	    break
	}
      }
    } else {
      actionOverlay.style.display="none"
    }
  }
  resize()
}

function ExpertiseName(number) {
  if (number==0) {
    return "-"
  }
  return "<em>"+expertises[(((gameNumber-1)*participantsNumber)+(number-1))%expertises.length]+"</em>"
}

//Update contact label of participant <uid>. If <reading> is true question and expertise are updated.
function updateContactLabel(uid,reading) {
  if (typeof(uid)!="number") {throw 'WrongTypeError: Expected number. Value is: '+JSON.stringify(uid)}
  uid=(uid==ownUid)?0:uid;
  if (uid==0) {
    var ownLabels=document.getElementById("ContactUser").getElementsByTagName('p')
    ownLabels[1].innerHTML="Your Question:<br>"+ExpertiseName(userData[ownUid].Question)
    ownLabels[2].innerHTML="Your Expertise:<br>"+ExpertiseName(userData[ownUid].Expertise)
    document.getElementById("Money").innerHTML="<b>"+reward+" Dkk</b>"
    document.getElementById("ActionSkipButton").disabled=(messagesLeft>0)?false:true;
    document.getElementById("MessagesLeft").setAttribute('src',(messagesLeft>0)?'Send.svg':'None.svg')
    return
  }
  var contact=document.getElementById("Contact"+uid)
  var labels=contact.getElementsByClassName('ContactLabel')
  labels[3].innerHTML=userData[uid].Name
  if (reading){
    labels[4].innerHTML="Question: "+ExpertiseName(userData[uid].Question)
    labels[5].innerHTML="Expertise: "+ExpertiseName(userData[uid].Expertise)
    if (userData[uid].ReferredTo!=null) {
      expertiseData[userData[ownUid].Question]=userData[uid].ReferredTo
      userData[userData[uid].ReferredTo].Expertise=userData[ownUid].Question
      document.getElementById("Contact"+userData[uid].ReferredTo).getElementsByClassName('ContactLabel')[5].innerHTML="Expertise: "+ExpertiseName(userData[ownUid].Question)
    }
  }
  //Choose which icons are visible
  labels[0].style.visibility=(userData[uid].NewMessages)?'visible':'hidden';
  labels[1].style.visibility=(userData[uid].SentInquire)?'hidden':'visible';
  if (userData[uid].ReceivedInquire) {
    //Choose which reply icon is shown
    switch (expertiseData[userData[uid].Question]) {
      case undefined:
	labels[2].setAttribute('src','Negate.svg');
	labels[2].style.visibility=(userData[uid].SentNegate)?'hidden':'visible';
	break;
      case ownUid:
	labels[2].setAttribute('src','Confirm.svg');
	labels[2].style.visibility=(userData[uid].SentConfirm)?'hidden':'visible';
	break;
      default:
	labels[2].setAttribute('src','Refer.svg');
	labels[2].style.visibility=(userData[uid].SentRefer)?'hidden':'visible';
	break;
    }
  } else {
    labels[2].style.visibility='hidden'
  }
  //Indicate if label is focused
  contact.style.backgroundColor=(uid==focusedUid)?'#FFE135':null;
}

function handleReward(event){
  var beneficiary=(event.hasOwnProperty('ReceiverUid'))?event.ReceiverUid:event.ReferredUid;
  if (!event.hasOwnProperty('Reward') || beneficiary!=ownUid) {
    return
  }
  var message=document.getElementById('RewardTemplate').cloneNode(true)
  message.removeAttribute('id')
  var rewardText = message.getElementsByTagName('p')[0]
  rewardText.innerHTML=" "+event.Reward+" Dkk "
  switch(event.EventId){
    case EventActionInquire:
      rewardText.innerHTML+=" (Inquiry)"
      break
    case EventActionConfirm:
      rewardText.innerHTML+=" (Confirmation)"
      break
    case EventActionRefer:
      rewardText.innerHTML+=" (Referral)"
      break
    case EventActionNegate:
      rewardText.innerHTML+=" (Negation)"
      break
    case EventNextGame:
      rewardText.innerHTML+=" (New game)"
      break
    case EventNextRound:
      rewardText.innerHTML+=" (New round)"
      break
  }
  rewardOverview.appendChild(message)
  reward+=event.Reward
  updateContactLabel(ownUid)
}

function handleSystemMessage(event){
  messagesLeft=1
  var message;
  if (event.EventId==EventNextRound) {
    message=document.getElementById('NextRoundTemplate').cloneNode(true)
  } else if (event.EventId==EventNextGame){
    gameNumber++
    // Things to do in the randomized experiment
    if (randomized) {
      shuffleContactOrder()
      assignNewContactNames()
      resetContactHistories()
      focusChat(0)
    }
    message=document.getElementById('NextGameTemplate').cloneNode(true)
    message.getElementsByTagName('p')[0].innerHTML="New Game! Your  question is: "+ExpertiseName(event.Question)+". Your expertise is "+ExpertiseName(event.Expertise)+"."
    expertiseData={}
    questionsData={}
  }
  message.removeAttribute('id')
  
  rewardOverview.appendChild(message.cloneNode(true))
  for (var uid in userData) {
    if (Number(uid)!=ownUid) {
      userData[uid].Chat.appendChild(message.cloneNode(true))
    }
    if (event.EventId==EventNextGame){
      if (Number(uid)==ownUid) {
	userData[ownUid].Expertise=event.Expertise
	expertiseData[event.Expertise]=ownUid;
	userData[ownUid].Question=event.Question
	questionsData[event.Question]=ownUid;
      } else {
	userData[uid].Expertise=0
	userData[uid].Question=0
	userData[uid].SentNewMessages=false
	userData[uid].SentInquire=false
	userData[uid].ReceivedInquire=false
	userData[uid].SentNegate=false
	userData[uid].SentRefer=false
	userData[uid].SentConfirm=false
	userData[uid].ReferredTo=null
	updateContactLabel(Number(uid),true)
      }
    }
  }
  updateActionsPanel()
  updateContactLabel(ownUid,true)
}

function handleMessage(event){
  var message;
  var chat;
  var ownMsg = (event.SenderUid==ownUid)
  var receiverName = 'you';
  var senderName = 'You';
  if (event.EventId!=EventActionSkip) {
    if (ownMsg) {
      //Check if message was valid
      if (event.Invalid) {
	invalidMessage = event
	updateActionsPanel()
	return
      }
      message = document.getElementById('MessageFromMeTemplate').cloneNode(true)
      chat = userData[event.ReceiverUid].Chat
      receiverName=userData[event.ReceiverUid].Name
    } else {
      message = document.getElementById('MessageToMeTemplate').cloneNode(true)
      chat = userData[event.SenderUid].Chat
      senderName=userData[event.SenderUid].Name
    }
    message.removeAttribute('id')
    var messageTitle = message.getElementsByTagName('p')[0]
    var messageText = message.getElementsByTagName('p')[1]
    var messageIcon = message.getElementsByTagName('img')[0]
    messageTitle.innerHTML='<b>'+senderName+' sent '+receiverName+'</b>'
    switch (event.EventId) {
      case EventActionInquire:
	messageTitle.innerHTML+="<b> an inquiry:</b>"
	messageText.innerHTML="I am an expert in "+ExpertiseName(event.Expertise)+". I have a question about "+ExpertiseName(event.Question)+". Can you help me?"
	messageIcon.setAttribute('src','Inquire.svg')
	break;
      case EventActionConfirm:
	messageTitle.innerHTML+="<b> a confirmation:</b>"
	messageText.innerHTML="I am the expert you are looking for. I will help you."
	messageIcon.setAttribute('src','Confirm.svg')
	break;
      case EventActionRefer:
	messageTitle.innerHTML+="<b> a referral:</b>"
	messageText.innerHTML="The expert you are looking for is <b>"+userData[event.ReferredUid].Name+"<b>."
	messageIcon.setAttribute('src','Refer.svg')
	break;
      case EventActionNegate:
	messageTitle.innerHTML+="<b> a negation:</b>"
	messageText.innerHTML="I am not the expert you are looking for and I don't know who your expert is."
	messageIcon.setAttribute('src','Negate.svg')
	break;
    }
    chat.appendChild(message)
    chat.scrollTop=chat.scrollHeight
  }
  
  if (ownMsg) {
    messagesLeft--
    //Handle message to other player
    if (event.EventId!=EventActionSkip) {
      switch (event.EventId) {
	case EventActionInquire:
	  userData[event.ReceiverUid].SentInquire=true;
	  break;
	case EventActionRefer:
	  userData[event.ReceiverUid].SentRefer=true;
	  break;
	case EventActionConfirm:
	  userData[event.ReceiverUid].SentConfirm=true;
	  break;
	case EventActionNegate:
	  userData[event.ReceiverUid].SentNegate=true;
	  break;
      }
      updateContactLabel(event.ReceiverUid)
    }
  } else {
    //Handle message from other player
    var senderData = userData[event.SenderUid];
    switch (event.EventId) {
      case EventActionInquire:
	senderData.ReceivedInquire=true
	senderData.Question=event.Question
	questionsData[event.Question]=event.SenderUid
	senderData.Expertise=event.Expertise
	expertiseData[event.Expertise]=event.SenderUid
	var questioner=questionsData[senderData.Expertise]
	if (questioner != undefined && questioner!=ownUid) {
	  updateContactLabel(questioner)
	}
	break;
      case EventActionRefer:
	userData[event.SenderUid].ReferredTo=event.ReferredUid
	break;
      case EventActionConfirm:
	senderData.Expertise=userData[ownUid].Question
	expertiseData[userData[ownUid].Question]=event.SenderUid;
	break;
    }
    if (!senderData.NewMessages && event.SenderUid!=focusedUid) {
      chatsUnread++
    }
    senderData.NewMessages=true;
    updateContactLabel(event.SenderUid,(event.SenderUid==focusedUid))
  }
  updateActionsPanel()
  updateContactLabel(0)
}
