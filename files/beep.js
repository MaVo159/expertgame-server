EventActionInquire = 1
EventActionConfirm = 2
EventActionRefer = 3
EventActionNegate = 4
EventActionSkip = 5
EventNextRound = 100
EventNextGame = 101
EventUserJoined = 1000

CommandEndRound = 100001
CommandStartGame = 100002
CommandAddUser =100003
CommandInjectAction = 100004
CommandToggleAutostartGames = 100005
CommandSetNextNumberRounds = 100006

RewardConfirm = 30
RewardNewGame = -15
RewardJoin = 380

participants={}
game=0
round=0
numberRounds=0
totalReward=0
nextNumberRounds=0
oneBeepTimer=null
twoBeepTimer=null
endRoundTimer=null

window.onload = function () {
    cmdRoundIn=document.getElementById('CmdRound')
    cmdActionTypeIn=document.getElementById('CmdActionType')
    cmdActionSenderIn = document.getElementById('CmdActionSender')
    cmdActionReceiverIn = document.getElementById('CmdActionReceiver')
    cmdActionReferredIn = document.getElementById('CmdActionReferred')
    cmdTypeIn = document.getElementById('CmdType')
    cmdGameIn=document.getElementById('CmdGame')
    cmdTypeIn.onchange=updateCommand
    cmdRoundIn.onchange=updateCommand
    cmdGameIn.onchange=updateCommand
    cmdActionReceiverIn.onchange=updateCommand
    cmdActionSenderIn.onchange=updateCommand
    cmdActionReferredIn.onchange=updateCommand
    cmdActionTypeIn.onchange=updateCommand
    
    document.getElementById('SetAutomaticRoundEndActive').onchange=scheduleEndRound
    document.getElementById("SetAutomaticNewGame").onclick=function(event){ws.send(JSON.stringify({CommandId:CommandToggleAutostartGames,CurrentGame:game,CurrentRound:round}));event.preventDefault()}
    document.getElementById("SetNextNumberRounds").onchange=function(event){
      ws.send(JSON.stringify({CommandId:CommandSetNextNumberRounds,CurrentGame:game,CurrentRound:round,NextRounds:document.getElementById("SetNextNumberRounds").valueAsNumber}));
      document.getElementById("SetNextNumberRounds").style.backgroundColor='#FFFF00';
      document.getElementById("SetNextNumberRounds").value=nextNumberRounds;
    }
  updateCommand()
  updateGeneralInformation()
  ws = new WebSocket("wss://"+window.location.host+"/beepWS");
  ws.onclose = function() {
    document.getElementById('ConnectionStatus').innerHTML="Disconnected... ...please reload"
    document.getElementById('ConnectionStatus').style.backgroundColor="red"
  }
  ws.onopen = function(){
    document.getElementById('ConnectionStatus').innerHTML="Connected"
    document.getElementById('ConnectionStatus').style.backgroundColor="green"
    ws.onmessage=function(msg){
      var allData=JSON.parse(msg.data);
      console.log(allData)
      if (allData.hasOwnProperty('Event') && allData.Event.EventId!=0){
      var msgData=allData.Event;
      if (msgData.EventId==EventNextRound || msgData.EventId==EventNextGame){
	scheduleBeeps()
      }
      
      
      //Handle message
      var logMessage = document.createElement('p')
      switch (msgData.EventId) {
	case EventActionInquire:
	  logMessage.innerHTML+='<b>Inquire:</b> ';
          if (!msgData.hasOwnProperty('Invalid') || msgData.Invalid==false) {
	  participants[msgData.SenderUid].SentMessage=true
	  }
	  generateParticipantLabel(msgData.SenderUid)
	  break;
	case EventActionConfirm:
	  logMessage.innerHTML+='<b>Confirm:</b> ';
	  if (!msgData.hasOwnProperty('Invalid') || msgData.Invalid==false) {
	  participants[msgData.SenderUid].SentMessage=true
	  }
	  generateParticipantLabel(msgData.SenderUid)
	  break;
	case EventActionRefer:
	  logMessage.innerHTML+='<b>Refer:</b> ';
	  if (!msgData.hasOwnProperty('Invalid') || msgData.Invalid==false) {
	  participants[msgData.SenderUid].SentMessage=true
	  }
	  generateParticipantLabel(msgData.SenderUid)
	  break;
	case EventActionNegate:
	  logMessage.innerHTML+='<b>Negate:</b> '; 
	  if (!msgData.hasOwnProperty('Invalid') || msgData.Invalid==false) {
	  participants[msgData.SenderUid].SentMessage=true
	  }
	  generateParticipantLabel(msgData.SenderUid)
	  break;
	case EventActionSkip:
	  logMessage.innerHTML+='<b>Skip</b> '; 
	  if (!msgData.hasOwnProperty('Invalid') || msgData.Invalid==false) {
	  participants[msgData.SenderUid].SentMessage=true
	  }
	  generateParticipantLabel(msgData.SenderUid)
	  break;
	case EventNextRound:
	  logMessage.innerHTML+='<b>Next Round:</b> ';
	  for (part in participants) {
	    if (participants.hasOwnProperty(part)) {
	      part=Number(part)
	      participants[part].SentMessage=false
	      generateParticipantLabel(part)
	    }
	  }
	  game=allData.Settings.StartedGames
	  round=allData.Settings.FinishedRounds
	  scheduleEndRound()
	  break;
	case EventNextGame:
	  logMessage.innerHTML+='<b>Next Game:</b> ';
	  participants[msgData.ReceiverUid].SentMessage=false
	  participants[msgData.ReceiverUid].Question=msgData.Question
	  participants[msgData.ReceiverUid].Expertise=msgData.Expertise
	  generateParticipantLabel(msgData.ReceiverUid)
	  game=allData.Settings.StartedGames
	  round=allData.Settings.FinishedRounds
	  scheduleEndRound()
	  break;
	case EventUserJoined:
	  logMessage.innerHTML+='<b>User joined:</b> ';
	  participants[msgData.ReferredUid]={SentMessage:false,Question:0,Expertise:0,Reward:0}
	  var participantLabel=document.createElement('p')
	  participantLabel.id='ParticipantLabel'+msgData.ReferredUid
	  document.getElementById('ParticipantList').appendChild(participantLabel)
	  generateParticipantLabel(msgData.ReferredUid)
	  break;
	default: logMessage.innerHTML+='<b>EventId '+msgData.EventId+":</b> "; break;
      }
      if (msgData.hasOwnProperty('SenderUid')) {logMessage.innerHTML+='Sender: '+msgData.SenderUid+' '}
      if (msgData.hasOwnProperty('ReceiverUid')) {logMessage.innerHTML+='Receiver: '+msgData.ReceiverUid+' '}
      if (msgData.hasOwnProperty('ReferredUid')) {logMessage.innerHTML+='Referred: '+msgData.ReferredUid+' '}
      if (msgData.hasOwnProperty('Question')) {logMessage.innerHTML+='Question: '+msgData.Question+' '}
      if (msgData.hasOwnProperty('Expertise')) {logMessage.innerHTML+='Expertise: '+msgData.Expertise+' '}
      if (msgData.hasOwnProperty('Reward')) {
	logMessage.innerHTML+='<span style="color: orange">Reward: '+msgData.Reward+'</span> '
	var beneficiary=(msgData.hasOwnProperty('ReceiverUid'))?msgData.ReceiverUid:msgData.ReferredUid;
	participants[beneficiary].Reward+=msgData.Reward
	totalReward+=msgData.Reward
	generateParticipantLabel(beneficiary)
      }
      if (msgData.hasOwnProperty('Invalid')) {logMessage.innerHTML+='<span style="color: red">Invalid: '+msgData.Invalid+'</span> '}
      if (msgData.hasOwnProperty('Admin')) {logMessage.innerHTML+='<span style="color: blue">Admin: '+msgData.Admin+'</span> '}
      if (msgData.hasOwnProperty('Time')) {logMessage.innerHTML+='Time: '+msgData.Time+' '}
      document.getElementById('EventLog').appendChild(logMessage)
    }
    //Process settings
    document.getElementById('TotalReward').innerHTML="Total reward: "+totalReward+" (surplus: "+(totalReward-RewardJoin*allData.Settings.ActiveParticipants-((RewardConfirm+RewardNewGame*2)*allData.Settings.StartedGames*allData.Settings.ActiveParticipants)/2)+" )"
    document.getElementById("SetAutomaticNewGame").checked=allData.Settings.AutostartNewGame
    game=allData.Settings.StartedGames
    round=allData.Settings.FinishedRounds
    numberRounds=allData.Settings.NumberRounds
    nextNumberRounds=allData.Settings.NextNumberRounds
    document.getElementById("SetNextNumberRounds").value=nextNumberRounds
    document.getElementById("SetNextNumberRounds").style.backgroundColor=null
    updateGeneralInformation()
    updateCommand()
    }
  }
};

function updateGeneralInformation() {
  var progress=document.getElementById("Progress")
  if (game==0) {
    progress.innerHTML="No game in progress (First game will have "+numberRounds+" rounds)."
  } else {
    progress.innerHTML=game+". game in progress. "+round+" rounds finished out of "+numberRounds+"."
  }
}

function updateCommand(){
  var sendButton=document.getElementById('CmdSend')
  var command = {}
  document.getElementById('Cmd'+CommandAddUser).disabled=(game>0)
  document.getElementById('Cmd'+CommandEndRound).disabled=(game==0)
  document.getElementById('Cmd'+CommandInjectAction).disabled=(game==0)  
  var cmdType=Number(cmdTypeIn.value)
  command['CommandId']=cmdType
  command['CurrentGame']=cmdGameIn.valueAsNumber
  cmdGameIn.style.backgroundColor=(command.CurrentGame==game)?null:'orange';
  command['CurrentRound']=cmdRoundIn.valueAsNumber
  cmdRoundIn.style.backgroundColor=(command.CurrentRound==round)?null:'orange';
  document.getElementById('CmdAction').style.display='none'
  if (cmdType==CommandInjectAction){
    document.getElementById('CmdAction').style.display='block'
    var cmdActionType=Number(cmdActionTypeIn.value)
    command['Event']={}
    command['Event']['EventId']=cmdActionType
    command['Event']['SenderUid']=document.getElementById('CmdActionSender').valueAsNumber
    if (cmdActionType==EventActionSkip) {
      document.getElementById('CmdActionReceiver').disabled=true
      cmdActionReferredIn.disabled=true
    } else {
      document.getElementById('CmdActionReceiver').disabled=false
      command['Event']['ReceiverUid']=document.getElementById('CmdActionReceiver').valueAsNumber
      if (cmdActionType==EventActionRefer) {
	cmdActionReferredIn.disabled=false
	command['Event']['ReferredUid']=document.getElementById('CmdActionReferred').valueAsNumber
      } else {
	cmdActionReferredIn.disabled=true
      }
    }
    var cmdActionSenderInOk=participants.hasOwnProperty(command.Event.SenderUid);
    cmdActionSenderIn.style.backgroundColor=(cmdActionSenderInOk)?null:'orange';
    var cmdActionReceiverInOk = participants.hasOwnProperty(command.Event.ReceiverUid)||(cmdActionType==EventActionSkip);
    cmdActionReceiverIn.style.backgroundColor=(cmdActionReceiverInOk)?null:'orange';
    var cmdActionReferredInOk = participants.hasOwnProperty(command.Event.ReferredUid)||(cmdActionType!=EventActionRefer);
    cmdActionReferredIn.style.backgroundColor=(cmdActionReferredInOk)?null:'orange';
  };
  console.log(command)
  sendButton.disabled=(document.getElementById('Cmd'+cmdType).disabled || command.CurrentGame!=game || command.CurrentRound != round || (cmdType==CommandInjectAction && (!cmdActionSenderInOk || !cmdActionReceiverInOk || !cmdActionReferredInOk)))
  sendButton.onclick=(sendButton.disabled)?null:function(){ws.send(JSON.stringify(command))};
}

function generateParticipantLabel(uid) {
  if (typeof(uid)!="number") {throw 'WrongTypeError: Expected number. Value is: '+JSON.stringify(uid)}
  var participantLabel=document.getElementById('ParticipantLabel'+uid)
  participantLabel.innerHTML="Uid: "+uid+" SentMessage: "+participants[uid].SentMessage+" Question: "+participants[uid].Question+" Expertise: "+participants[uid].Expertise+" Reward: "+participants[uid].Reward
}

function scheduleEndRound() {
  clearTimeout(endRoundTimer)
  if (document.getElementById('SetAutomaticRoundEndActive').checked) {
    var _copy_round = round
    var _copy_game = game
    endRoundTimer=setTimeout(function(){ws.send(JSON.stringify({CommandId:CommandEndRound,CurrentGame:_copy_game,CurrentRound:_copy_round}))},document.getElementById('SetAutomaticRoundEndDelay').valueAsNumber*1000)
  }
}

function scheduleBeeps() {
  clearTimeout(oneBeepTimer)
  clearTimeout(twoBeepTimer)
  var oneBeepDelay = document.getElementById('SetOneBeepRoundSound').valueAsNumber
  if (oneBeepDelay==0) {
    document.getElementById('OneBeepRoundSound').play()
  } else if (oneBeepDelay>0) {
    oneBeepTimer=setTimeout(function(){document.getElementById('OneBeepRoundSound').play()},oneBeepDelay*1000)
  }
  var twoBeepDelay = document.getElementById('SetTwoBeepRoundSound').valueAsNumber
  if (document.getElementById('SetTwoBeepRoundSound').valueAsNumber==0) {
    document.getElementById('TwoBeepRoundSound').play()
  } else if (document.getElementById('SetTwoBeepRoundSound').valueAsNumber>0) {
    twoBeepTimer=setTimeout(function(){document.getElementById('TwoBeepRoundSound').play()},twoBeepDelay*1000)
  }
}
