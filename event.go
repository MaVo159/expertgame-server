package main

import "time"

const (
	EventActionInquire = 1
	EventActionConfirm = 2
	EventActionRefer   = 3
	EventActionNegate  = 4
	EventActionSkip    = 5
	EventNextRound     = 100
	EventNextGame      = 101
	EventUserJoined    = 1000
)

const (
	CommandEndRound     = 100001
	CommandStartGame    = 100002
	CommandAddUser      = 100003
	CommandInjectAction = 100004
	CommandToggleAutostartGames = 100005
	CommandSetNextNumberRounds = 100006
)

type Event struct {
	EventId     int64
	SenderUid   int64 `json:",omitempty"`
	ReceiverUid int64 `json:",omitempty"`
	ReferredUid int64 `json:",omitempty"`
	Question    int64 `json:",omitempty"`
	Expertise   int64 `json:",omitempty"`
	Time        time.Time
	Reward      int64 `json:",omitempty"`
	Admin       bool  `json:",omitempty"`
	Invalid     bool  `json:",omitempty"`
}

type BeepEvent struct {
  Event Event
  Settings Settings
}

type Command struct {
	CommandId    int64
	CurrentRound int64
	CurrentGame  int64
	NextRounds    int64 `json:",omitempty"`
	Time         time.Time
	Event        Event `json:",omitempty"`
}
