package main

import "sync"
import "math/rand"
import "time"
import "fmt"
import "encoding/json"
import "io"

type Game struct {
	Lock                                                                      sync.RWMutex `json:"-"`
	Participants                                                              ParticipantList
	toEngine                                                                  chan Event   `json:"-"`
	toBeep                                                                    []chan BeepEvent `json:"-"`
	History                                                                   []Event `json:"-"`
	HistoryWriter                                                             io.Writer `json:"-"`
	Settings	Settings
}

type Settings struct{
	AutostartNewGame                                                          bool
	ActionsLeft, ActiveParticipants, LoggedInParticipants                     int64
	FinishedRounds, NumberRounds, StartedGames, TotalReward, NextNumberRounds int64
}

const RewardConfirm = 30
const RewardNewGame = -15
const RewardJoin = 380

func (g *Game) CanonicalizeAction(event Event) Event {
	//Receiver Field
	if event.EventId == EventActionSkip {
		event.ReceiverUid = 0
	}
	//Referred Field
	if event.EventId != EventActionRefer {
		event.ReferredUid = 0
	}
	//Question and Expertise Fields
	if event.EventId == EventActionInquire {
		sender := g.Participants[event.SenderUid]
		event.Question = sender.Question
		event.Expertise = sender.Expertise
	} else {
		event.Question = 0
		event.Expertise = 0
	}
	return event
}

func (g *Game) HistoryAppend(event Event) {
	g.History = append(g.History, event)
	enc, err := json.Marshal(event)
	if err == nil {
		fmt.Fprintln(g.HistoryWriter, string(enc))
	} else {
		fmt.Println("Error marshalling event for history:", err)
	}
	g.BroadcastToBeep(event)
}

func (g *Game) BroadcastToBeep(event Event) {
	//Send it out to beep channels
	for _, c := range g.toBeep {
		if c != nil {
			c <- BeepEvent{Event:event,Settings:g.Settings}
		}
	}
}
  
func (g *Game) SetActionReward(event Event) Event {
	event.Reward = 0
	if event.EventId == EventActionConfirm {
		event.Reward = RewardConfirm
		idx := len(g.History) - 1
		for ; g.History[idx].EventId != EventNextGame; idx-- {
			if g.History[idx].Invalid {continue}
			if g.History[idx].EventId == EventActionConfirm && g.History[idx].ReceiverUid == event.ReceiverUid {
				event.Reward = 0
				break
			}
		}
	}
	g.Settings.TotalReward += event.Reward
	return event
}

func (g *Game) ValidateAction(event Event) bool {
	sender := g.Participants[event.SenderUid]
	receiver := g.Participants[event.ReceiverUid]
	referred := g.Participants[event.ReferredUid]
	//Check if sender and receiver are existing players
	if sender == nil {
		fmt.Println("ValidateActionError: Unknown sender")
		return false
	}
	if event.EventId != EventActionSkip {
		if receiver == nil {
			fmt.Println("ValidateActionError: Unknown receiver")
			return false
		}
		if event.SenderUid == event.ReceiverUid {
			fmt.Println("ValidateActionError: Sender and receiver are identical")
			return false
		}
	}
	//Refer: Check if referred player is active and distict from sender and receiver.
	if event.EventId == EventActionRefer && (referred == nil) {
		fmt.Println("ValidateActionError: Refer: Unknown referred")
		return false
	}
	if event.EventId == EventActionRefer && (event.SenderUid == event.ReferredUid) {
		fmt.Println("ValidateActionError: Refer: Referred and sender are identical")
		return false
	}
	if event.EventId == EventActionRefer && (event.ReferredUid == event.ReceiverUid) {
		fmt.Println("ValidateActionError: Refer: Referred and receiver are identical")
		return false
	}
	//Confirm: Check if sender is expert for receiver.
	if event.EventId == EventActionConfirm && sender.Expertise != receiver.Question {
		fmt.Println("ValidateActionError: Confirm: Question of receiver does not match expertise of sender")
		return false
	}
	//Negate: Check if sender is not expert for receiver.
	if event.EventId == EventActionNegate && sender.Expertise == receiver.Question && sender.Expertise != 0 {
		fmt.Println("ValidateActionError: Negate: Question of receiver matches expertise of sender")
		return false
	}
	//Refer: Check if referred player is expert for receiver.
	if event.EventId == EventActionRefer && referred.Expertise != receiver.Question {
		fmt.Println("ValidateActionError: Refer: Question of receiver does not match expertise of referred")
		return false
	}
	//Inquire and Skip: Check if the game has started
	if event.EventId == EventActionInquire && event.EventId == EventActionSkip {
		for idx := len(g.History) - 1; g.History[idx].EventId != EventNextGame; idx-- {
			if idx == 0 {
				//The game has not started yet. All messages are invalid.
				fmt.Println("ValidateActionError: Inquire or Skip: Inquire or Skip before game started")
				return false
			}
		}
	}
	//Reply: Check if game has progressed to second round. Check if receiver sent an Inquire to
	//sender during this game, excluding the current round. Refer: Check if sender got Inquire from
	//referred person. Negate: Check if sender got Inquire from the requested expert.
	if event.EventId == EventActionConfirm || event.EventId == EventActionNegate || event.EventId == EventActionRefer {
		idx := len(g.History) - 1
		//After checking history, these two booleans should be false
		missingInquireFromReceiver := true
		missingInquireFromReferred := event.EventId == EventActionRefer
		//Skip current round. Stop if it is the first one.
		for ; g.History[idx].EventId != EventNextRound; idx-- {
			if g.History[idx].EventId == EventNextGame {
				//Current round is the first in this game. All replies are invalid during the first round.
				fmt.Println("ValidateActionError: Reply: Reply during first round")
				return false
			}
			if idx == 0 {
				//The game has not started yet. All messages are invalid.
				fmt.Println("ValidateActionError: Reply: Reply before game started")
				return false
			}
		}
		//Check for Inquires from receiver to sender and from referred to sender
		for ; g.History[idx].EventId != EventNextGame; idx-- {
			if g.History[idx].Invalid {continue}
			if g.History[idx].EventId == EventActionInquire && g.History[idx].ReceiverUid == event.SenderUid {
				if g.History[idx].SenderUid == event.ReceiverUid {
					missingInquireFromReceiver = false
				}
				if event.EventId == EventActionRefer && g.History[idx].SenderUid == event.ReferredUid {
					missingInquireFromReferred = false
				}
				if event.EventId == EventActionNegate {
					potentialRefer := g.Participants[g.History[idx].SenderUid]
					if potentialRefer == nil {
						fmt.Println("ValidateActionWarning: Negate: Encountered unknown potential refer. This should not happen.")
					} else if receiver.Question == potentialRefer.Expertise {
						fmt.Println("ValidateActionError: Negate: Got an Inquire from the requested expert")
						return false
					}
				}
				if !missingInquireFromReceiver && !missingInquireFromReferred && event.EventId != EventActionNegate {
					break
				}
			}
		}
		if missingInquireFromReceiver {
			fmt.Println("ValidateActionError: Reply: No Inquire from receiver")
			return false
		}
		if missingInquireFromReferred {
			fmt.Println("ValidateActionError: Refer: No Inquire from referred")
			return false
		}
	}
	return true
}

//Send last round, if there was one
func (g *Game) SendLastRound() {
	for startIdx := len(g.History) - 1; startIdx >= 0; startIdx-- {
		if g.History[startIdx].EventId == EventNextGame || g.History[startIdx].EventId == EventNextRound {
			for _, event := range g.History[startIdx:len(g.History)] {
				if event.Invalid {continue}
				//Send it to receiver
				if 0 < event.EventId && event.EventId <= EventActionNegate {
					g.Participants[event.ReceiverUid].Send(event)
				}
			}
			break
		}
	}
}

func (g *Game) StartNewRound() {
	event := Event{EventId: EventNextRound, Time: time.Now()}
	for _, userData := range g.Participants {
		userData.ActionsLeft = 1
		g.Settings.ActionsLeft++
	}
	g.Broadcast(event)
	g.HistoryAppend(event)
}

func (g *Game) StartNewGame() {
	if g.Settings.ActiveParticipants < 2 {
		fmt.Println("StartGameError: There are less than 2 active participants")
		return
	}
	//Calculate number of rounds for the next game
	if g.Settings.NextNumberRounds == 0 {
		//Check if total reward is above expectation (Expectation: Players win half of the games)
	        surplus:=g.Settings.TotalReward-RewardJoin*g.Settings.ActiveParticipants-((RewardConfirm+RewardNewGame*2)*g.Settings.StartedGames*g.Settings.ActiveParticipants)/2
		switch {
		  case surplus>0 && g.Settings.NumberRounds>2 :
		    g.Settings.NumberRounds--
		  case surplus<0 && g.Settings.NumberRounds<14:
		    g.Settings.NumberRounds++
		}
	} else {
		g.Settings.NumberRounds = g.Settings.NextNumberRounds
	}
	//Generate pairs
	var questions, expertise []int
Drawing:
	for {
		questions = rand.Perm(int(g.Settings.ActiveParticipants))
		expertise = rand.Perm(int(g.Settings.ActiveParticipants))
		//Make sure that current draw doesn't have matching question and expertise in same position
		for idx := range questions {
			if questions[idx] == expertise[idx] {
				continue Drawing
			}
		}
		break
	}

	idx := 0
	g.Settings.FinishedRounds = 0
	g.Settings.StartedGames++
	for uid, userData := range g.Participants {
		userData.ActionsLeft = 1
		g.Settings.ActionsLeft++
		userData.Question = int64(questions[idx] + 1)
		userData.Expertise = int64(expertise[idx] + 1)
		event := Event{EventId: EventNextGame, ReceiverUid: uid, Question: int64(questions[idx] + 1), Expertise: int64(expertise[idx] + 1), Time: time.Now(), Reward:RewardNewGame}
		g.Settings.TotalReward += event.Reward
		userData.Send(event)
		g.HistoryAppend(event)
		idx++
	}
	g.BroadcastToBeep(Event{})
}

func (p *Participant) Send(event Event) {
	if p.toClient != nil {
		p.toClient <- event
	}
}

func (g *Game) Broadcast(event Event) {
	for _, p := range g.Participants {
		p.Send(event)
	}
}
